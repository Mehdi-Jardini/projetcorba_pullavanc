package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import DialoguePackage.member;
import pullavance.DialogueImpl;
import pullavance.Serveur;



public class DialogueImplTest {

	@Test
	public void testGetClients() {
		DialogueImpl dial = new DialogueImpl();
		String [] clients = new String[2];
		 clients [0]= "Mehdi";
		 clients [1]= "Jardini";
		member newmember1 = new member();
		newmember1.message="pas de message";
		newmember1.pseudo="mehdi";
		
		member newmember2 = new member();
		newmember2.message="pas de message";
		newmember2.pseudo="Jardini";
		
		Serveur.myMembers.add(newmember1);
		Serveur.myMembers.add(newmember2);
		
		
		String []clientsRecus =new String[Serveur.myMembers.size()];
		clientsRecus = dial.getClients();
		Assert.assertNotEquals("c'est une erreur", null, clientsRecus);
	}

	@Test
	public void testGetMessages() {
		DialogueImpl dial = new DialogueImpl();

		member newmember1 = new member();
		newmember1.message="message1";
		newmember1.pseudo="mehdi";
		
		member newmember2 = new member();
		newmember2.message="message2";
		newmember2.pseudo="Jardini";
		
		Serveur.myMembers.add(newmember1);
		Serveur.myMembers.add(newmember2);
		
		
		String []messages =new String[Serveur.myMembers.size()];
		messages = dial.getMessages();
		Assert.assertNotEquals("c'est une erreur", null, messages);
	}

	@Test
	public void testGetMembers() {
		DialogueImpl dial  = new DialogueImpl();
		member member1 = new member();
		member member2 = new member();
		
		Serveur.myMembers.add(member1);
		Serveur.myMembers.add(member2);

		member[] listMember = dial.getMembers();
		Assert.assertNotEquals("c'est une erreur", null, listMember);
		}
	}

