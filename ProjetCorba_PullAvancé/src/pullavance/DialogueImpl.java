package pullavance;

import java.util.ArrayList;

import DialoguePackage.member;

public class DialogueImpl extends DialoguePOA {

	public  static String pseudoInstance;

	
	public member myMember;
	

	public DialogueImpl(){};
	
	public DialogueImpl(String pseudo) {
		
		this.pseudoInstance = pseudo;
		member newMember = new member();
		newMember.message = " ";
		newMember.pseudo = pseudo;
		Serveur.myMembers.add(newMember);
	}

	@Override
	public String[] getClients() {

		member [] list = getMembers();
		
		String[] clients = new String[list.length];
		int i = 0;

		for (member tmp : list) {
			clients[i] = tmp.pseudo;
			i++;
		}

		return clients;

	}

	@Override
	public String[] getMessages() {
		String[] messages = new String[Serveur.myMembers.size()];
		int i = 0;

		for (member tmp : Serveur.myMembers)
			{
				messages[i] =tmp.message;
				i++;
			}

		return messages;
	}

	@Override
	public void sendMessage(String pseudo, String message) {

		member newMember = new member();
		newMember.pseudo=pseudo;
		newMember.message=message;
		
		System.out.println(pseudoInstance+" a envoy� � "+pseudo+" le message suivant :"+ message);
		
		Serveur.myMembers.add(newMember);
		
	}

	@Override
	public member[] getMembers() {
		member[] listMember = new member[Serveur.myMembers.size()];
		int j = 0;
		for (member tmp : Serveur.myMembers) {
			listMember[j] = tmp;
			j++;
		}
		return listMember;
	}

}
