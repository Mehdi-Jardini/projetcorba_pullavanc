package pullavance;

import java.util.ArrayList;

import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

public class ConnectionImpl extends ConnectionPOA {

	public ArrayList<DialogueImpl> dialogues = new ArrayList<DialogueImpl>();

	
	@Override
	public Dialogue connect(String nickname) {
		DialogueImpl dial ;

		Dialogue dialogue =null;
		System.out.println("ajout Client");
		dial = new DialogueImpl(nickname);

		
		try {
			dialogue = DialogueHelper.narrow(_default_POA().servant_to_reference(dial));
		} catch (ServantNotActive | WrongPolicy e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return dialogue;
	}

	@Override
	public void disconnect(String nickname) {

		for ( DialogueImpl tmp : dialogues)
			if( tmp.pseudoInstance.equals(nickname))
				dialogues.remove(tmp);
				
	}

}
