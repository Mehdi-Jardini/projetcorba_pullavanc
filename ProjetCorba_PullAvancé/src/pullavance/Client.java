package pullavance;

import java.util.ArrayList;
import java.util.Scanner;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;


import org.omg.CORBA.ORBPackage.InvalidName;

public class Client {
	
	public static String pseudo;
	

	public static void main(String args[]) {
		java.util.Properties props = System.getProperties();

		int status = 0;
		org.omg.CORBA.ORB orb = null;

		try {
			orb = ORB.init(args, props);
			run(orb);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = 1;
		}

		if (orb != null) {
			try {
				orb.destroy();
			} catch (Exception ex) {
				ex.printStackTrace();
				status = 1;
			}
		}

		System.exit(status);
	}

	static void run(ORB orb) {
		org.omg.CORBA.Object obj = null;

		try {

			obj = orb.resolve_initial_references("NameService");
		} catch (InvalidName e) {
			e.printStackTrace();
			System.exit(1);
		}

		NamingContext ctx = NamingContextHelper.narrow(obj);

		if (ctx == null) {
			System.out.println("Le composant NameService n'est pas un repertoire");
			System.exit(1);
		}

		NameComponent[] name = new NameComponent[1];

		name[0] = new NameComponent("Hello", "");

		try {
			obj = ctx.resolve(name);
		} catch (Exception e) {
			System.out.println("Composant inconnu");
			e.printStackTrace();
			System.exit(1);
		}

		/*
		 * String refFile = "Hello.ref"; java.io.BufferedReader in = new
		 * java.io.BufferedReader( new java.io.FileReader(refFile)); String ref
		 * = in.readLine(); System.out.println("IOR :"+ref); obj =
		 * orb.string_to_object(ref); //obj =
		 * orb.string_to_object("relfile:/Hello.ref");
		 */

//		Hello hello = HelloHelper.narrow(obj);
//		System.out.println("Entrer votre pseudo pour vous connecter");
//		Scanner sc = new Scanner(System.in);
//		String pseudo = sc.nextLine();
		Connection connection = ConnectionHelper.narrow(obj);
		Dialogue dialogue = null;
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer votre Identifiant s'il vous plait :");
		pseudo = sc.nextLine();
		dialogue=connection.connect(pseudo);
		Menu(dialogue);
		
		
//		System.out.println("Les clients du chat sont : ");
//		String[] clients = dial.getClients();
//
//		for( int k = 0; k<clients.length;k++)
//			System.out.println(clients [k]);
//		
//		System.out.println("Entrer le pseudo du destinataire");
//		String username =sc.nextLine();
//		System.out.println("Entrer votre message");
//		String message =sc.nextLine();
//		dial.sendMessage(username, message);


//		System.out.println("Les messages qui vous ont �t� adress� sont : ");
//		String[] messages = new String [100];
//		messages = dial.getMessages();
//		for( int k = 0; k<messages.length;k++)
//			System.out.println(messages [k]);
		
		
		

//		System.out.println(hello.SayHello());

	}
	
	public static void Menu(Dialogue dialogue) {
		

		
		System.out.println(" Bienvenue dans le chat - Pull Avanc�e choisissez une action");
		System.out.println("*-------------------------------------------*");
		System.out.println("b - Visualiser les utilisateurs du chat ");
		System.out.println("c - Envoyer un message � un utilisateur du chat ");
		System.out.println("d - Afficher les messages � votre intention ");
		System.out.println("q - Quitter le chat ");

		Scanner sc = new Scanner(System.in);
		String sto = sc.nextLine();

		switch (sto) {

		case "b":
			System.out.println("Visualiser les clients du chat ");
			String[] Clients = dialogue.getClients();
			for (int j = 0; j < Clients.length; j++) {
				System.out.println(Clients[j]);

			}
			Menu(dialogue);
		case "c":
			System.out.println("Entrer le pseudo de votre destinataire s'il vous plait :");
			String receiver = sc.nextLine();
			System.out.println("Entrer le message :");
			String message = sc.nextLine();
			dialogue.sendMessage(receiver, message);
			Menu(dialogue);

		case "d":
			String[] messages =  dialogue.getMessages();
			for (int i = 0; i < messages.length; i++)
				if(messages[i].contains(pseudo))
				System.out.println(messages[i]);
			Menu(dialogue);

		case "q":
			System.out.println(" A toute ! ");
			break;
		}

	}


}
