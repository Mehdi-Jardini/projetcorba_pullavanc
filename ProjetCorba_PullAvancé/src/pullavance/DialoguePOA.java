package pullavance;


/**
* DialoguePOA.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from Dialogue.idl
* lundi 25 janvier 2016 00 h 22 CET
*/

public abstract class DialoguePOA extends org.omg.PortableServer.Servant
 implements DialogueOperations, org.omg.CORBA.portable.InvokeHandler
{

  // Constructors

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static
  {
    _methods.put ("sendMessage", new java.lang.Integer (0));
    _methods.put ("getClients", new java.lang.Integer (1));
    _methods.put ("getMessages", new java.lang.Integer (2));
    _methods.put ("getMembers", new java.lang.Integer (3));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method,
                                org.omg.CORBA.portable.InputStream in,
                                org.omg.CORBA.portable.ResponseHandler $rh)
  {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ())
    {
       case 0:  // Dialogue/sendMessage
       {
         String pseudo = in.read_string ();
         String message = in.read_string ();
         this.sendMessage (pseudo, message);
         out = $rh.createReply();
         break;
       }

       case 1:  // Dialogue/getClients
       {
         String $result[] = null;
         $result = this.getClients ();
         out = $rh.createReply();
         DialoguePackage.StringsHelper.write (out, $result);
         break;
       }

       case 2:  // Dialogue/getMessages
       {
         String $result[] = null;
         $result = this.getMessages ();
         out = $rh.createReply();
         DialoguePackage.StringsHelper.write (out, $result);
         break;
       }

       case 3:  // Dialogue/getMembers
       {
         DialoguePackage.member $result[] = null;
         $result = this.getMembers ();
         out = $rh.createReply();
         DialoguePackage.MembersHelper.write (out, $result);
         break;
       }

       default:
         throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  } // _invoke

  // Type-specific CORBA::Object operations
  private static String[] __ids = {
    "IDL:Dialogue:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public Dialogue _this() 
  {
    return DialogueHelper.narrow(
    super._this_object());
  }

  public Dialogue _this(org.omg.CORBA.ORB orb) 
  {
    return DialogueHelper.narrow(
    super._this_object(orb));
  }


} // class DialoguePOA
